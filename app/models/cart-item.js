import DS from "ember-data";
const { Model } = DS;

export default Model.extend({
  quantity: DS.attr("number"),
  product: DS.belongsTo("product"),
});
