import product from "../app/models/product";

export default function () {
  this.namespace = "/api";

  let products = [
    { id: 1, name: "Product 1", price: 10, categoryId: 1 },
    { id: 2, name: "Product 2", price: 20, categoryId: 2 },
  ];

  let categories = [
    { id: 1, name: "Category 1" },
    { id: 2, name: "Category 2" },
  ];

  let cartItems = [
    { id: 1, quantity: 12, productId: 1 },
    { id: 2, quantity: 16, productId: 2 },
  ];

  this.get("/products", function (db, request) {
    if (request.queryParams.categoryId !== undefined) {
      let filteredProducts = products.filter(function (i) {
        return (
          i.attributes.categoryId
            .toLowerCase()
            .indexOf(request.queryParams.categoryId) !== -1
        );
      });
      return { data: filteredProducts };
    } else {
      return { data: products };
    }
  });

  this.get("/products/:id", function (db, request) {
    const productId = request.params.id;
    return { data: products.find((product) => productId === product.id) };
  });

  this.get("/categories", function () {
    return { data: categories };
  });

  this.get("/categories/:id", function (db, request) {
    const categoryId = request.params.id;
    return { data: categories.find((category) => categoryId === category.id) };
  });

  this.post("/cart-items", function () {
    return { data: cartItems };
  });

  this.patch("/cart-items/:id", function (db, request) {
    const cartItemId = request.params.id;
    return { data: cartItems.filter((cartItem) => cartItem.id !== cartItemId) };
  });

  this.del("/cart-items/:id", function (db, request) {
    const cartItemId = request.params.id;
    return cartItems.find(cartItemId).destroy();
  });

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `/api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');

    https://www.ember-cli-mirage.com/docs/route-handlers/shorthands
  */
}
